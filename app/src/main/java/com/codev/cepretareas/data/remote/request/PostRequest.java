package com.codev.cepretareas.data.remote.request;

import com.codev.cepretareas.data.entities.UserByJob;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by kath on 25/01/18.
 */

public interface PostRequest {

    @POST("calificar/problema/{idproblema}/{puntaje}")
    Call<Void> sendCalification(@Field("idproblema") int idproblema,
                                @Field("puntaje") int puntaje);

}
