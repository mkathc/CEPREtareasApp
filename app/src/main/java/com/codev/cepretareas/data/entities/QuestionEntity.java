package com.codev.cepretareas.data.entities;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by kath on 2/06/18.
 */

public class QuestionEntity implements Serializable {
    private int id;
    private String imgProblema;
    private String imgSolucion;
    private String titulo;
    private String descripcion;
    private double calificacion;
    private int nro_calificaciones;
    private int idCiclo;
    private String nombreCiclo;
    private int idSemana;
    private String nombreSemana;
    private int idTarea;
    private String nombreTarea;
    private int idCurso;
    private String nombreCurso;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgProblema() {
        return imgProblema;
    }

    public void setImgProblema(String imgProblema) {
        this.imgProblema = imgProblema;
    }

    public String getImgSolucion() {
        return imgSolucion;
    }

    public void setImgSolucion(String imgSolucion) {
        this.imgSolucion = imgSolucion;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }

    public int getNro_calificaciones() {
        return nro_calificaciones;
    }

    public void setNro_calificaciones(int nro_calificaciones) {
        this.nro_calificaciones = nro_calificaciones;
    }

    public int getIdCiclo() {
        return idCiclo;
    }

    public void setIdCiclo(int idCiclo) {
        this.idCiclo = idCiclo;
    }

    public String getNombreCiclo() {
        return nombreCiclo;
    }

    public void setNombreCiclo(String nombreCiclo) {
        this.nombreCiclo = nombreCiclo;
    }

    public int getIdSemana() {
        return idSemana;
    }

    public void setIdSemana(int idSemana) {
        this.idSemana = idSemana;
    }

    public String getNombreSemana() {
        return nombreSemana;
    }

    public void setNombreSemana(String nombreSemana) {
        this.nombreSemana = nombreSemana;
    }

    public int getIdTarea() {
        return idTarea;
    }

    public void setIdTarea(int idTarea) {
        this.idTarea = idTarea;
    }

    public String getNombreTarea() {
        return nombreTarea;
    }

    public void setNombreTarea(String nombreTarea) {
        this.nombreTarea = nombreTarea;
    }

    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }
}
