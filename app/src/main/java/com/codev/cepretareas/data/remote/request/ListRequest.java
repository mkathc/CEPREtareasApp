package com.codev.cepretareas.data.remote.request;

import com.codev.cepretareas.data.entities.NotificationEntity;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.entities.UserByJob;
import com.codev.cepretareas.data.entities.trackholder.TrackEntityHolder;
import com.codev.cepretareas.data.entities.trackholder.TrackEntityHolderUser;


import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


/**
 * Created by katherine on 12/06/17.
 */

public interface ListRequest {

    @GET("listado/Ejercicios/{usuario}/{ciclo}")
    Call<ArrayList<QuestionEntity>> getListQuestions(@Path("usuario") int usuario,
                                                     @Path("ciclo") int ciclo);

    @GET("ultimas/notificaciones/{idciclo}")
    Call<ArrayList<NotificationEntity>> getNotifications(@Path("idciclo") int idciclo);



}
