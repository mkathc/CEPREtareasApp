package com.codev.cepretareas.data.entities.trackholder;

import java.util.ArrayList;

/**
 * Created by miguel on 7/04/17.
 */

public class TrackEntityHolder <T> {
    private ArrayList<T> jobs;

    public ArrayList<T> getJobs() {
        return jobs;
    }

    public void setJobs(ArrayList<T> jobs) {
        this.jobs = jobs;
    }
}
