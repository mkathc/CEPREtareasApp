package com.codev.cepretareas.data.entities;

import java.io.Serializable;

/**
 * Created by kath on 2/06/18.
 */

public class CourseEntity implements Serializable {
    private int idCurso;
    private String nombreCurso;


    public int getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombreCurso() {
        return nombreCurso;
    }

    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }
}
