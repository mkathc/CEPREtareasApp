package com.codev.cepretareas.data.remote.request;

import com.codev.cepretareas.data.entities.AccessTokenEntity;
import com.codev.cepretareas.data.entities.UserEntity;
import com.codev.cepretareas.data.entities.body.BodyRegister;
import com.codev.cepretareas.data.entities.response.ResponseRegister;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;

/**
 * Created by katherine on 10/05/17.
 */

public interface RegisterRequest {

    @FormUrlEncoded
    @POST("insertar/usuario")
    Call<ResponseRegister> registerUser(@Body BodyRegister bodyRegister);
}

