package com.codev.cepretareas.presentation.auth;

import android.content.Context;
import android.support.annotation.NonNull;

import com.codev.cepretareas.data.entities.AccessTokenEntity;
import com.codev.cepretareas.data.entities.UserEntity;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.data.remote.ServiceFactory;
import com.codev.cepretareas.data.remote.request.LoginRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by katherine on 10/05/17.
 */

public class LoginPresenter implements LoginContract.Presenter {

    private final LoginContract.View mView;
    private Context context;
    private final SessionManager mSessionManager;

    public LoginPresenter(@NonNull LoginContract.View mView, @NonNull Context context) {
        this.context = checkNotNull(context, "context cannot be null!");
        this.mView = checkNotNull(mView, "newsView cannot be null!");
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }

    @Override
    public void loginUser(final String username, final String password) {
        LoginRequest loginService =
                ServiceFactory.createService(LoginRequest.class);
        Call<ArrayList<UserEntity>> call = loginService.loginUser();
        mView.setLoadingIndicator(true);
        call.enqueue(new Callback<ArrayList<UserEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<UserEntity>> call, Response<ArrayList<UserEntity>> response) {
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {

                    switch (response.code()) {
                        case 200:
                            getUser(username, password, response.body());
                            //openSession(response.body(), response.body()());
                            break;
                        case 202:
                            mView.setLoadingIndicator(false);
                            mView.errorLogin("Verifica si tu cuenta es de administrador o usuario");
                            break;

                    }
                } else {
                    mView.setLoadingIndicator(false);
                    mView.errorLogin("Usuario o contraseña incorrectos");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.errorLogin("No se puede conectar al servidor");
            }
        });
    }

    private void getUser(String username, String password, ArrayList<UserEntity> list) {

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getEmail().equals(username) && list.get(i).getPass().equals(password)) {
                openSession(list.get(i));
            }
        }
    }

    @Override
    public void openSession(UserEntity userEntity) {

        // mSessionManager.openSession(token);
        mSessionManager.setUser(userEntity);
        mSessionManager.setLastUser(userEntity.getEmail());
        mView.setLoadingIndicator(false);
        mView.loginSuccessful(userEntity);
    }

    @Override
    public void start() {

    }
}
