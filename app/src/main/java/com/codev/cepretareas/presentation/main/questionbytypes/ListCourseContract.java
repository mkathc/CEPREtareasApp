package com.codev.cepretareas.presentation.main.questionbytypes;


import com.codev.cepretareas.core.BasePresenter;
import com.codev.cepretareas.core.BaseView;

/**
 * Created by katherine on 31/05/17.
 */

public interface ListCourseContract {
    interface View extends BaseView<Presenter> {

        void clickItemCourse();

        boolean isActive();



    }

    interface Presenter extends BasePresenter {

       // void loadListHomework(int idUser, int idCiclo);

    }
}
