package com.codev.cepretareas.presentation.main.detailquestion;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.codev.cepretareas.R;
import com.codev.cepretareas.core.BaseActivity;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.utils.ActivityUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by kath on 2/06/18.
 */

public class DetailQuestionActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.body)
    FrameLayout body;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    private SessionManager mSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back);
        ButterKnife.bind(this);
        mSessionManager = new SessionManager(getApplicationContext());

        QuestionEntity questionEntity = (QuestionEntity) getIntent().getExtras().get("questionEntity");
        toolbar.setTitle(questionEntity.getNombreCurso() +" #"+ questionEntity.getId());

        DetailQuestionFragment fragment = (DetailQuestionFragment) getSupportFragmentManager()
                .findFragmentById(R.id.body);

        if (fragment == null) {
            fragment = DetailQuestionFragment.newInstance(getIntent().getExtras());

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.body);
        }
    }


}
