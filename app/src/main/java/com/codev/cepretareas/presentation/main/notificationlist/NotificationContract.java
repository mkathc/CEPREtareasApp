package com.codev.cepretareas.presentation.main.notificationlist;


import com.codev.cepretareas.core.BasePresenter;
import com.codev.cepretareas.core.BaseView;
import com.codev.cepretareas.data.entities.NotificationEntity;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.entities.Week;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public interface NotificationContract {
    interface View extends BaseView<Presenter> {

     //   void getListHomework(ArrayList<QuestionEntity> list);

        void getListNotification(ArrayList<NotificationEntity> list);

        boolean isActive();


    }

    interface Presenter extends BasePresenter {

        void getListNotification(int idCiclo);

    }
}
