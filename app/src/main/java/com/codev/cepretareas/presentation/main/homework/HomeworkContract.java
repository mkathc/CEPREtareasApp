package com.codev.cepretareas.presentation.main.homework;


import com.codev.cepretareas.core.BasePresenter;
import com.codev.cepretareas.core.BaseView;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.entities.Week;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public interface HomeworkContract {
    interface View extends BaseView<Presenter> {

     //   void getListHomework(ArrayList<QuestionEntity> list);

        void getListWeek(ArrayList<Week> list);

        void getListHomeworkByWeekAndType(ArrayList<QuestionEntity> list);

        boolean isActive();


    }

    interface Presenter extends BasePresenter {

        void loadListHomework(int idUser, int idCiclo);

        void loadListWeek(ArrayList<QuestionEntity> list);

        void loadListHomeworkbyWeekAndType(int type, int idWeek);

    }
}
