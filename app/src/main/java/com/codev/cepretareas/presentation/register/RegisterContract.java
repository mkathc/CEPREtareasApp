package com.codev.cepretareas.presentation.register;


import android.support.annotation.NonNull;

import com.codev.cepretareas.core.BasePresenter;
import com.codev.cepretareas.core.BaseView;
import com.codev.cepretareas.data.entities.UserEntity;
import com.codev.cepretareas.data.entities.body.BodyRegister;
import com.codev.cepretareas.data.entities.response.ResponseRegister;

/**
 * Created by katherine on 3/05/17.
 */

public interface RegisterContract {
    interface View extends BaseView<Presenter> {

        void registerSuccessful(ResponseRegister responseRegister);
        void errorRegister(String msg);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void registerUser(@NonNull BodyRegister bodyRegister);

    }
}
