package com.codev.cepretareas.presentation.main.homework;

import android.content.Context;
import android.widget.ArrayAdapter;


import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.entities.Week;
import com.codev.cepretareas.data.entities.trackholder.TrackEntityHolder;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.data.remote.ServiceFactory;
import com.codev.cepretareas.data.remote.request.ListRequest;
import com.codev.cepretareas.data.remote.request.PostRequest;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 31/05/17.
 */

public class HomeworkPresenter implements HomeworkContract.Presenter {

    private final HomeworkContract.View mView;
    private final SessionManager mSessionManager;
    private Context context;
    private boolean firstLoad = false;
    private int currentPage = 1;

    private ArrayList<Week> listWeek = new ArrayList<>();
    private ArrayList<QuestionEntity> listQuestions = new ArrayList<>();

    public HomeworkPresenter(HomeworkContract.View mView, Context context) {
        this.mView = mView;
        this.mSessionManager = new SessionManager(context);
        this.mView.setPresenter(this);
    }

    @Override
    public void start() {
    }

    @Override
    public void loadListHomework(int idUser, int idCiclo) {
        mView.setLoadingIndicator(true);
        final ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        final Call<ArrayList<QuestionEntity>> reservation = listRequest.getListQuestions(idUser, idCiclo);
        reservation.enqueue(new Callback<ArrayList<QuestionEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<QuestionEntity>> call, Response<ArrayList<QuestionEntity>> response) {
                mView.setLoadingIndicator(false);
                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    listQuestions = response.body();
                        if(listWeek.size() == 0){
                            loadListWeek(listQuestions);

                        }else   {
                            mView.getListWeek(listWeek);
                        }
                    //mView.getListHomework(response.body());

                } else {
                    mView.showErrorMessage("Error al obtener la lista");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<QuestionEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void loadListWeek(ArrayList<QuestionEntity> list) {

        boolean first = true;
        Week week = new Week();


        for (int i = 0; i < list.size(); i++) {
            if (first) {
                week.setId(list.get(i).getIdSemana());
                week.setNameWeek(list.get(i).getNombreSemana());
                listWeek.add(week);
                first = false;
            } else {
                if (list.get(i).getIdSemana() != week.getId()) {
                    Week newWeek = new Week();
                    newWeek.setId(list.get(i).getIdSemana());
                    newWeek.setNameWeek(list.get(i).getNombreSemana());
                    listWeek.add(newWeek);
                    week = newWeek;
                }
            }
        }

        mView.getListWeek(listWeek);

    }

    @Override
    public void loadListHomeworkbyWeekAndType(int type, int idWeek) {
        ArrayList<QuestionEntity> list = new ArrayList<>();
        for (int i = 0; i <listQuestions.size() ; i++) {
            if(listQuestions.get(i).getIdSemana()== idWeek && listQuestions.get(i).getIdTarea() == type){
                list.add(listQuestions.get(i));
            }
        }
        mView.getListHomeworkByWeekAndType(list);
    }


}
