package com.codev.cepretareas.presentation.main.questionbytypes;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codev.cepretareas.R;
import com.codev.cepretareas.core.BaseFragment;
import com.codev.cepretareas.data.entities.CourseEntity;
import com.codev.cepretareas.data.entities.QuestionEntity;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.presentation.main.homework.HomeworkContract;
import com.codev.cepretareas.presentation.profile.ProfileActivity;
import com.codev.cepretareas.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by katherine on 31/05/17.
 */

public class QuestionbyTypesFragment extends BaseFragment {


    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.refresh_layout)
    ConstraintLayout refreshLayout;
    Unbinder unbinder;
    private HomeworkContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;

    private SessionManager mSessionManager;
    private AlertDialog dialogSend;

    private ArrayList<QuestionEntity> list, listByCourse;
    private ArrayList<CourseEntity> listCourse;

    private ListCourseAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;


    public QuestionbyTypesFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public static QuestionbyTypesFragment newInstance(Bundle bundle) {
        QuestionbyTypesFragment fragment = new QuestionbyTypesFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_homework_types, container, false);
        list = (ArrayList<QuestionEntity>) getArguments().get("list");


        unbinder = ButterKnife.bind(this, root);
        return root;
    }
    //CAMBIE EL ORDEN DE LAS DECLARACIONES PARA GUIARME DE MIS EJEMPLOS ANTERIORES
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Obteniendo datos...");
        listCourse = new ArrayList<>();
        //CAMBIE EL PRIMER ARGUMENTO, PERO NO RECUERDO POR QUE
        mAdapter = new ListCourseAdapter(listCourse, getContext(), list, getActivity());
        mLayoutManager = new LinearLayoutManager(getContext());
        rvList.setLayoutManager(mLayoutManager);
        rvList.setAdapter(mAdapter);
        getListCourse(list);

    }

    private void getListCourse(ArrayList<QuestionEntity> list){
        boolean first = true;
        CourseEntity course = new CourseEntity();

        for (int i = 0; i < list.size(); i++) {
            if (first) {
                course.setIdCurso(list.get(i).getIdCurso());
                course.setNombreCurso(list.get(i).getNombreCurso());
                listCourse.add(course);
                first = false;
            } else {
                if (list.get(i).getIdCurso() != course.getIdCurso()) {
                    CourseEntity newCourse = new CourseEntity();
                    newCourse.setIdCurso(list.get(i).getIdCurso());
                    // CAMBIE NOMBRE SEMANA POR NOMBRE CURSO
                    newCourse.setNombreCurso(list.get(i).getNombreCurso());
                    listCourse.add(newCourse);
                    course = newCourse;
                }
            }
        }

        mAdapter.setItems(listCourse);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_profile:
                //Toast.makeText(getActivity(), "Favoritos", Toast.LENGTH_SHORT).show();
                nextActivity(getActivity(), null, ProfileActivity.class, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }


}
