package com.codev.cepretareas.presentation.profile;

import com.codev.cepretareas.core.BasePresenter;
import com.codev.cepretareas.core.BaseView;
import com.codev.cepretareas.data.entities.UserEntity;

import java.io.File;

/**
 * Created by katherine on 21/06/17.
 */

public interface ProfileContract {
    interface View extends BaseView<Presenter> {
        void ShowSessionInformation(UserEntity userEntity);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

    }
}
