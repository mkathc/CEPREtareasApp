package com.codev.cepretareas.presentation.main.notificationlist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.codev.cepretareas.R;
import com.codev.cepretareas.core.BaseActivity;
import com.codev.cepretareas.core.BaseFragment;
import com.codev.cepretareas.data.entities.NotificationEntity;
import com.codev.cepretareas.data.local.SessionManager;
import com.codev.cepretareas.presentation.profile.ProfileActivity;
import com.codev.cepretareas.utils.ProgressDialogCustom;

import java.util.ArrayList;

/**
 * Created by katherine on 31/05/17.
 */

public class NotificationFragment extends BaseFragment implements NotificationContract.View {



    private NotificationContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;

    private SessionManager mSessionManager;
    public NotificationFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getListNotification(mSessionManager.getUserEntity().getIdCiclo());
    }

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        mPresenter = new NotificationPresenter(this, getContext());
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_list_notification, container, false);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Obteniendo datos...");


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_profile:
                //Toast.makeText(getActivity(), "Favoritos", Toast.LENGTH_SHORT).show();
                nextActivity(getActivity(), null, ProfileActivity.class, false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void getListNotification(ArrayList<NotificationEntity> list) {

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(NotificationContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        if (getView() == null) {
            return;
        }
    }

    @Override
    public void showMessage(String message) {
        ((BaseActivity) getActivity()).showMessage(message);
    }

    @Override
    public void showErrorMessage(String message) {
        ((BaseActivity) getActivity()).showMessageError(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
